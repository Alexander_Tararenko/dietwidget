package com.gmail.tarlorien.dietwidget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.appwidget.AppWidgetManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

/**
 * Configuration activity
 */
public class ConfigActivity extends Activity implements View.OnClickListener {

    int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    Intent resultValue;

    private static final int TIME_DIALOG_ID = 101;
    private static final int HELP_DIALOG_ID = 102;
    private static final int REQUEST_CODE_RINGTONE = 1001;

    private Button mOkButton;
    private Button mStartButton;
    private Button mStopButton;
    private Button mHelpButton;
    private CheckBox mRingtoneCheck;
    private CheckBox mNotificationCheck;
    private TextView mTime;
    private Button mRingtone;
    private int hours;
    private int minutes;
    private SharedPreferences sp;

    public final static String WIDGET_PREF = "widget_pref";
    public final static String WIDGET_MINUTES_CONFIG = "widget_minutes_config_";
    public final static String WIDGET_MINUTES_REMAINING = "widget_minutes_remaining_";
    public final static String WIDGET_MINUTES_STARTED = "widget_minutes_started_";
    public final static String WIDGET_STATUS = "widget_status_";
    public final static String WIDGET_VIEW_STATUS = "widget_view_status_";
    public final static String WIDGET_RINGTONE_URI = "widget_ringtone_uri_";
    public final static String WIDGET_NEED_EXPAND = "widget_expand_";
    public final static String WIDGET_RINGTONE_ENABLED = "widget_ringtone_enabled_";
    public final static String WIDGET_NOTIFICATION_ENABLED = "widget_notification_enabled_";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        if (widgetID == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
        resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        setResult(RESULT_CANCELED, resultValue);
        sp = getSharedPreferences(WIDGET_PREF, MODE_PRIVATE);
        setContentView(R.layout.config);

        mTime = (TextView) findViewById(R.id.time);
        mTime.setOnClickListener(this);
        mRingtoneCheck = (CheckBox) findViewById(R.id.ringtone_check);
        mRingtoneCheck.setChecked(sp.getBoolean(WIDGET_RINGTONE_ENABLED + widgetID, true));
        mRingtoneCheck.setOnClickListener(this);
        mNotificationCheck = (CheckBox) findViewById(R.id.notification_check);
        mNotificationCheck.setChecked(sp.getBoolean(WIDGET_RINGTONE_ENABLED + widgetID, true));
        mNotificationCheck.setOnClickListener(this);
        mRingtone = (Button) findViewById(R.id.ringtone);
        mRingtone.setEnabled(sp.getBoolean(WIDGET_NOTIFICATION_ENABLED + widgetID, true));
        mRingtone.setOnClickListener(this);
        mOkButton = (Button) findViewById(R.id.ok_button);
        mOkButton.setOnClickListener(this);
        mHelpButton = (Button) findViewById(R.id.help_button);
        mHelpButton.setOnClickListener(this);
        mStartButton = (Button) findViewById(R.id.start_button);
        mStartButton.setOnClickListener(this);
        mStopButton = (Button) findViewById(R.id.stop_button);
        mStopButton.setOnClickListener(this);
        hours = sp.getInt(WIDGET_MINUTES_CONFIG + widgetID, 120) / 60;
        minutes = sp.getInt(WIDGET_MINUTES_CONFIG + widgetID, 120) % 60;
        mTime.setText(String.format("%02d:%02d", hours, minutes));
        Uri ringtoneUri = Uri.parse(sp.getString(WIDGET_RINGTONE_URI + widgetID,
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()));
        mRingtone.setText(getFileNameFromUri(ringtoneUri));
    }

    @Override
    public void onClick(View view) {
        SharedPreferences.Editor editor = sp.edit();
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(ConfigActivity.this);
        switch (view.getId()) {
            case R.id.time:
                showDialog(TIME_DIALOG_ID);
                break;
            case R.id.ringtone:
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                Intent intent2 = new Intent(Intent.ACTION_GET_CONTENT);
                intent2.setType("audio/*");
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE,
                        this.getResources().getString(R.string.select_ringtone));
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                Intent chooserIntent = Intent.createChooser(intent,
                        this.getResources().getString(R.string.open_in));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {intent2});
                this.startActivityForResult(chooserIntent, REQUEST_CODE_RINGTONE);
                break;
            case R.id.ringtone_check:
                if (mRingtoneCheck.isChecked()) {
                    mRingtone.setEnabled(true);
                    editor.putBoolean(WIDGET_RINGTONE_ENABLED + widgetID, true);
                } else {
                    mRingtone.setEnabled(false);
                    editor.putBoolean(WIDGET_RINGTONE_ENABLED + widgetID, false);
                }
                editor.commit();
                break;
            case R.id.notification_check:
                if (mNotificationCheck.isChecked()) {
                    mRingtone.setEnabled(true);
                    mRingtoneCheck.setEnabled(true);
                    editor.putBoolean(WIDGET_NOTIFICATION_ENABLED + widgetID, true);
                } else {
                    mRingtone.setEnabled(false);
                    mRingtoneCheck.setEnabled(false);
                    editor.putBoolean(WIDGET_NOTIFICATION_ENABLED + widgetID, false);
                }
                editor.commit();
                break;
            case R.id.start_button:
                editor.putBoolean(ConfigActivity.WIDGET_STATUS + widgetID, true);
                editor.putInt(WIDGET_MINUTES_CONFIG + widgetID, 60 * hours + minutes);
                editor.commit();
                editor.putInt(ConfigActivity.WIDGET_MINUTES_REMAINING + widgetID,
                        sp.getInt(ConfigActivity.WIDGET_MINUTES_CONFIG + widgetID, 0));
                editor.putInt(ConfigActivity.WIDGET_MINUTES_STARTED + widgetID,
                        sp.getInt(ConfigActivity.WIDGET_MINUTES_CONFIG + widgetID, 0));
                editor.commit();
                DietWidgetProvider.disableNotification(ConfigActivity.this, widgetID);
                if (sp.getBoolean(ConfigActivity.WIDGET_NOTIFICATION_ENABLED + widgetID, true)) {
                    DietWidgetProvider.enableNotification(ConfigActivity.this, sp, widgetID);
                }
                Toast.makeText(this, R.string.task_started, Toast.LENGTH_SHORT).show();
                break;
            case R.id.stop_button:
                editor.putBoolean(ConfigActivity.WIDGET_STATUS + widgetID, false);
                editor.putInt(ConfigActivity.WIDGET_MINUTES_REMAINING + widgetID, 0);
                editor.commit();
                DietWidgetProvider.disableNotification(ConfigActivity.this, widgetID);
                Toast.makeText(this, R.string.task_stopped, Toast.LENGTH_SHORT).show();
                break;
            case R.id.help_button:
                showDialog(HELP_DIALOG_ID);
                break;
            case R.id.ok_button:
                editor.putInt(WIDGET_MINUTES_CONFIG + widgetID, 60 * hours + minutes);
                editor.putBoolean(WIDGET_VIEW_STATUS + widgetID, true);
                editor.putBoolean(WIDGET_NEED_EXPAND + widgetID, true);
                editor.commit();
                setResult(RESULT_OK, resultValue);
                DietWidgetProvider.updateWidget(ConfigActivity.this, appWidgetManager, sp, widgetID);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_RINGTONE) {
                Uri uri = data.getData();
                if (uri == null) {
                    uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                }
                if (uri != null) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString(WIDGET_RINGTONE_URI + widgetID, uri.toString());
                    editor.commit();
                    Uri ringtoneUri = Uri.parse(sp.getString(WIDGET_RINGTONE_URI + widgetID,
                            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION).toString()));
                    mRingtone.setText(getFileNameFromUri(ringtoneUri));
                }
            }
        }
    }


    private String getFileNameFromUri(Uri uri) {
        String fileName = "unknown";
        Uri filePathUri = uri;
        try {
            if (uri.getScheme().toString().compareTo("content") == 0) {
                Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
                if (cursor.moveToFirst()) {
                    int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
                    filePathUri = Uri.parse(cursor.getString(columnIndex));
                    fileName = filePathUri.getLastPathSegment().toString();
                }
            } else if (uri.getScheme().compareTo("file") == 0) {
                fileName = filePathUri.getLastPathSegment().toString();
            } else {
                fileName = fileName + "_" + filePathUri.getLastPathSegment();
            }
        } catch (NullPointerException e) {
            fileName = filePathUri.getLastPathSegment();
        } catch (IllegalArgumentException e) {
            fileName = filePathUri.getLastPathSegment();
        }
        return fileName;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this, mTimeSetListener, hours, minutes, true);
            case HELP_DIALOG_ID:
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.help,
                        (ViewGroup)findViewById(R.id.dialog_layout));
                AlertDialog.Builder adb = new AlertDialog.Builder(this);
                adb.setTitle(R.string.help);
                adb.setIcon(R.drawable.ic_launcher);
                adb.setView(layout);
                adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                return adb.create();
        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    hours = hourOfDay;
                    minutes = minute;
                    mTime.setText(String.format("%02d:%02d", hours, minutes));
                }
            };

}
