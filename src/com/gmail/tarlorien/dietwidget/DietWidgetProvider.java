package com.gmail.tarlorien.dietwidget;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.RemoteViews;

import java.util.Calendar;

public class DietWidgetProvider extends AppWidgetProvider {

    final static String UPDATE_ALL_WIDGETS = "com.gmail.tarlorien.dietwidget.update_all_widgets";
    final static String START_ACTION = "com.gmail.tarlorien.dietwidget.start_action";

    final static int NOTIFY_ID = 11;

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Intent intent = new Intent(context, DietWidgetProvider.class);
        intent.setAction(UPDATE_ALL_WIDGETS);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis(),
                60000, pIntent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        SharedPreferences sp = context.getSharedPreferences(
                ConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
        for (int id : appWidgetIds) {
            updateWidget(context, appWidgetManager, sp, id);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        SharedPreferences.Editor editor = context.getSharedPreferences(
                ConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE).edit();
        for (int widgetID : appWidgetIds) {
            editor.remove(ConfigActivity.WIDGET_MINUTES_REMAINING + widgetID);
            editor.remove(ConfigActivity.WIDGET_MINUTES_CONFIG + widgetID);
            editor.remove(ConfigActivity.WIDGET_MINUTES_STARTED + widgetID);
            editor.remove(ConfigActivity.WIDGET_STATUS + widgetID);
            editor.remove(ConfigActivity.WIDGET_VIEW_STATUS + widgetID);
            editor.remove(ConfigActivity.WIDGET_RINGTONE_URI + widgetID);
            disableNotification(context, widgetID);
        }
        editor.commit();
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Intent intent = new Intent(context, DietWidgetProvider.class);
        intent.setAction(UPDATE_ALL_WIDGETS);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        // updating all widgets
        if (intent.getAction().equalsIgnoreCase(UPDATE_ALL_WIDGETS)) {
            ComponentName thisAppWidget = new ComponentName(context.getPackageName(), getClass().getName());
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            SharedPreferences sp = context.getSharedPreferences(ConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
            int ids[] = appWidgetManager.getAppWidgetIds(thisAppWidget);
            SharedPreferences.Editor editor = sp.edit();
            for (int appWidgetID : ids) {
                int minutesRemaining = sp.getInt(ConfigActivity.WIDGET_MINUTES_REMAINING + appWidgetID, -1);
                editor.putInt(ConfigActivity.WIDGET_MINUTES_REMAINING + appWidgetID, minutesRemaining - 1);
                editor.commit();
                updateWidget(context, appWidgetManager, sp, appWidgetID);
            }
        }
        // start button clicked
        if (intent.getAction().equalsIgnoreCase(START_ACTION)) {
            int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
            Bundle extras = intent.getExtras();
            if (extras != null) {
                widgetID = extras.getInt(
                        AppWidgetManager.EXTRA_APPWIDGET_ID,
                        AppWidgetManager.INVALID_APPWIDGET_ID);
            }
            if (widgetID != AppWidgetManager.INVALID_APPWIDGET_ID) {
                SharedPreferences sp = context.getSharedPreferences(
                        ConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();

                if (!sp.getBoolean(ConfigActivity.WIDGET_STATUS + widgetID, false)) {
                    editor.putBoolean(ConfigActivity.WIDGET_NEED_EXPAND + widgetID, true);
                    if (sp.getBoolean(ConfigActivity.WIDGET_NOTIFICATION_ENABLED + widgetID, true)) {
                        enableNotification(context, sp, widgetID);
                    }
                    if (sp.getBoolean(ConfigActivity.WIDGET_VIEW_STATUS + widgetID, true)) {
                        editor.putBoolean(ConfigActivity.WIDGET_STATUS + widgetID, true);
                        editor.putInt(ConfigActivity.WIDGET_MINUTES_REMAINING + widgetID,
                                sp.getInt(ConfigActivity.WIDGET_MINUTES_CONFIG + widgetID, 0));
                        editor.putInt(ConfigActivity.WIDGET_MINUTES_STARTED + widgetID,
                                sp.getInt(ConfigActivity.WIDGET_MINUTES_CONFIG + widgetID, 0));
                    } else {
                        editor.putBoolean(ConfigActivity.WIDGET_VIEW_STATUS + widgetID, true);
                    }
                } else {
                    if (!sp.getBoolean(ConfigActivity.WIDGET_VIEW_STATUS + widgetID, true)) {
                        editor.putBoolean(ConfigActivity.WIDGET_NEED_EXPAND + widgetID, true);
                        editor.putBoolean(ConfigActivity.WIDGET_VIEW_STATUS + widgetID, true);
                    }
                }
                editor.commit();
                updateWidget(context, AppWidgetManager.getInstance(context), sp, widgetID);
            }
        }
    }

    static void updateWidget(Context context, AppWidgetManager appWidgetManager,
                             SharedPreferences sp, int widgetID) {
        SharedPreferences.Editor editor = sp.edit();
        if (!sp.getBoolean(ConfigActivity.WIDGET_NEED_EXPAND + widgetID, true)) {
            editor.putBoolean(ConfigActivity.WIDGET_VIEW_STATUS + widgetID, false);
            editor.commit();
        }
        int minutesRemaining = sp.getInt(ConfigActivity.WIDGET_MINUTES_REMAINING + widgetID, -1);
        // if time has passed
        if (minutesRemaining == 0 && sp.getBoolean(ConfigActivity.WIDGET_STATUS + widgetID, false)) {
            editor.putBoolean(ConfigActivity.WIDGET_STATUS + widgetID, false);
            editor.remove(ConfigActivity.WIDGET_MINUTES_STARTED + widgetID);
            editor.commit();
            disableNotification(context, widgetID);
            if (sp.getBoolean(ConfigActivity.WIDGET_RINGTONE_ENABLED, true)) {
                endTask(context, sp.getString(ConfigActivity.WIDGET_RINGTONE_URI + widgetID, null));
            }
        }

        if (sp.getBoolean(ConfigActivity.WIDGET_VIEW_STATUS + widgetID, true)) {
            updateExpandedUI(context, appWidgetManager, sp, widgetID);
        } else {
            updateReducedUI(context, appWidgetManager, sp, widgetID);
        }

        editor.putBoolean(ConfigActivity.WIDGET_NEED_EXPAND + widgetID, false);
        editor.commit();
    }

    private static void updateExpandedUI(Context context, AppWidgetManager appWidgetManager,
                                  SharedPreferences sp, int widgetID) {
        RemoteViews widgetViewExpanded = new RemoteViews(context.getPackageName(), R.layout.widget_expanded);
        SharedPreferences.Editor editor = sp.edit();
        Intent configIntent = new Intent(context, ConfigActivity.class);
        configIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE);
        configIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        PendingIntent pIntent = PendingIntent.getActivity(context, widgetID,
                configIntent, 0);
        widgetViewExpanded.setOnClickPendingIntent(R.id.pref_button, pIntent);

        Intent startIntent = new Intent(context, DietWidgetProvider.class);
        startIntent.setAction(START_ACTION);
        startIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        pIntent = PendingIntent.getBroadcast(context, widgetID, startIntent, 0);
        widgetViewExpanded.setOnClickPendingIntent(R.id.main_button_enabled, pIntent);
        widgetViewExpanded.setOnClickPendingIntent(R.id.main_button_disabled, pIntent);

        int minutesRemaining = sp.getInt(ConfigActivity.WIDGET_MINUTES_REMAINING + widgetID, -1);
        int minutesStarted = sp.getInt(ConfigActivity.WIDGET_MINUTES_STARTED + widgetID,
                sp.getInt(ConfigActivity.WIDGET_MINUTES_CONFIG + widgetID, 0));
        String buttonText = String.format("%02d:%02d", minutesStarted / 60, minutesStarted % 60);
        widgetViewExpanded.setTextViewText(R.id.main_button_enabled, buttonText);
        widgetViewExpanded.setTextViewText(R.id.main_button_disabled, buttonText);

        if (sp.getBoolean(ConfigActivity.WIDGET_STATUS + widgetID, false)) {
            String remaining = context.getResources().getString(R.string.remaining);
            String hours = context.getResources().getString(R.string.hours);
            String minutes = context.getResources().getString(R.string.minutes);
            String widgetText = remaining + "\n";
            if (minutesRemaining / 60 > 0) {
                widgetText += (minutesRemaining / 60) + " " + hours + " ";
            }
            widgetText += (minutesRemaining % 60) + " " + minutes;
            widgetViewExpanded.setTextViewText(R.id.remain_text, widgetText);
            widgetViewExpanded.setProgressBar(R.id.progressBarTimer, 100,
                    (minutesStarted - minutesRemaining) * 100 / minutesStarted, false);
            widgetViewExpanded.setViewVisibility(R.id.main_button_enabled, View.VISIBLE);
            widgetViewExpanded.setViewVisibility(R.id.main_button_disabled, View.INVISIBLE);
        } else {
            widgetViewExpanded.setTextViewText(R.id.remain_text, "");
            widgetViewExpanded.setProgressBar(R.id.progressBarTimer, 100, 0, false);
            widgetViewExpanded.setViewVisibility(R.id.main_button_enabled, View.INVISIBLE);
            widgetViewExpanded.setViewVisibility(R.id.main_button_disabled, View.VISIBLE);
        }
        editor.commit();
        appWidgetManager.updateAppWidget(widgetID, widgetViewExpanded);
    }

    private static void updateReducedUI(Context context, AppWidgetManager appWidgetManager,
                                  SharedPreferences sp, int widgetID) {
        RemoteViews widgetViewReduced = new RemoteViews(context.getPackageName(), R.layout.widget_reduced);
        Intent startIntent = new Intent(context, DietWidgetProvider.class);
        startIntent.setAction(START_ACTION);
        startIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, widgetID, startIntent, 0);
        widgetViewReduced.setOnClickPendingIntent(R.id.main_button_enabled2, pIntent);
        widgetViewReduced.setOnClickPendingIntent(R.id.main_button_disabled2, pIntent);

        int minutesStarted = sp.getInt(ConfigActivity.WIDGET_MINUTES_STARTED + widgetID,
                sp.getInt(ConfigActivity.WIDGET_MINUTES_CONFIG + widgetID, 0));
        String buttonText = String.format("%02d:%02d", minutesStarted / 60, minutesStarted % 60);
        widgetViewReduced.setTextViewText(R.id.main_button_enabled2, buttonText);
        widgetViewReduced.setTextViewText(R.id.main_button_disabled2, buttonText);

        if (sp.getBoolean(ConfigActivity.WIDGET_STATUS + widgetID, false)) {
            widgetViewReduced.setViewVisibility(R.id.main_button_enabled2, View.VISIBLE);
            widgetViewReduced.setViewVisibility(R.id.main_button_disabled2, View.INVISIBLE);
        } else {
            widgetViewReduced.setViewVisibility(R.id.main_button_enabled2, View.INVISIBLE);
            widgetViewReduced.setViewVisibility(R.id.main_button_disabled2, View.VISIBLE);
        }
        appWidgetManager.updateAppWidget(widgetID, widgetViewReduced);
    }

    static void enableNotification(Context context, SharedPreferences sp, int widgetID) {
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int icon = R.drawable.ic_launcher;
        CharSequence tickerText = context.getResources().getText(R.string.notification_title);
        long when = System.currentTimeMillis();
        int minutes = sp.getInt(ConfigActivity.WIDGET_MINUTES_STARTED + widgetID, 0);
        Calendar calendar = Calendar.getInstance();
        String message = String.format("%s.\n%02d:%02d - %02d:%02d",
                tickerText,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.HOUR_OF_DAY) + (minutes / 60),
                calendar.get(Calendar.MINUTE) + (minutes % 60));
        Intent notificationIntent = new Intent(context, DietWidgetProvider.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification);
        contentView.setImageViewResource(R.id.image, R.drawable.ic_launcher);
        contentView.setTextViewText(R.id.notification_text, message);
        Notification notification = new Notification(icon, tickerText, when);
        notification.contentIntent = contentIntent;
        notification.contentView = contentView;
        notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        mNotifyMgr.notify(NOTIFY_ID + widgetID, notification);
    }

    static void disableNotification(Context context, int widgetID) {
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.cancel(NOTIFY_ID + widgetID);
    }

    private static void endTask(Context context, String ringtoneUri) {
        Uri notificationUri;
            if (ringtoneUri == null) {
                notificationUri = RingtoneManager.getActualDefaultRingtoneUri(context,
                        RingtoneManager.TYPE_NOTIFICATION);
            } else {
                notificationUri = Uri.parse(ringtoneUri);
            }
        Ringtone r = RingtoneManager.getRingtone(context, notificationUri);
        r.play();
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(750);
    }

}
